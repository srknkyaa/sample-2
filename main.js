(function() {
    var mainObj = {
      attribute: {
        pageType: "",
        isLogin: false,
        basket: {
          amount: 0,
          count: 0
        },
        category: {
          categoryName: "",
          gender: ""
        },
        product: {
          productName: "",
          productCode: "",
          price: 0,
          color: "",
          size: ""
        }
  
      },
      methods: {
        isLogin: function() {
           
        },
        category: function(){
            mainObj.attribute.category.categoryName = $("#breadCrumbs_child_2 span").text().trim()
            mainObj.attribute.category.gender =$("#breadCrumbs_child_2 span").text().trim().split(" ")[0]
 
        },
        isLogin: function(){
            mainObj.attribute.isLogin = ($("li.register").length == 0) ? true : false;
 
        },
        basket: function(){
            var a = $("li.basket ul li").length;
            var y = $("div.hb-sum b").text();
            mainObj.attribute.basket.amount = parseFloat(y);
            mainObj.attribute.basket.count = a;
        },
        product: function(){
            if(mainObj.attribute.pageType === "Product"){
               mainObj.attribute.product.productName = $("h1.black-v1").text().trim();
               var c = $("ins.price-payable").text();
               mainObj.attribute.category.categoryName = $("#breadCrumbs_child_2 span").text().trim()
               mainObj.attribute.category.gender =$("#breadCrumbs_child_2 span").text().trim().split(" ")[0]
               mainObj.attribute.product.price = parseFloat(c);
               mainObj.attribute.product.color = $("span.color").text();
               mainObj.attribute.product.productCode = $("ul.productInfo").text().split(":")[1].split(" ")[1]
               mainObj.attribute.product.size = $("ul.productInfo").text().split(":")[2].split(" ")[1]
               
            } 
             
        },
        pageTypee: function() {
          var x = $(location).attr('pathname').split("-");
            var y = $("h1.black-v1").text().trim().split(" ");
            console.log( y[0]);
          for (var i = 0; i < x.length; i++) {
              var str = x[i];
              switch(str){
                
                case '/':
                  mainObj.attribute.pageType = "Home";
                  return;
                case 'modelleri':
                  mainObj.attribute.pageType = "Listing";
                  return;
                case 'c':
                  mainObj.attribute.pageType = "Category";
                  return;
                case '/sepet':
                  mainObj.attribute.pageType = "Basket";
                  return;
                case '/' + y[0].toLowerCase():
                  mainObj.attribute.pageType = "Product";
                  return;
              }
          }
          
        },
        isPrepare: function() {
          try{
              mainObj.methods.pageTypee();
              mainObj.methods.category();
              mainObj.methods.product();
              mainObj.methods.basket();
              mainObj.methods.isLogin();
          }catch(e){
              //console.error(e);
              //return;
          }

          
        }
      }
    }
  
    mainObj.methods.isPrepare();
    window.saint = mainObj;
  
  })();
