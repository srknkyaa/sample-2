(function(){

/**
Kullanıcı ürün sayfasında ise,
sepetinde en az 2 ürün var ise,
ürünü sepetine eklememiş ise,
sayfadan yukarı doğru çıkmaya çalıştığı zaman,

Popup
--------------------------------
                               X
             Ürün adı
            Ürün Fiyatı
           
           ---------------
           - Sepete Ekle -
           ---------------
           
--------------------------------

Ek olarak, 

Popup'ı gören bir daha aynı session içerisinde görmesin.
sepetindeki ürünlerin toplam tutarı 50 tl üstünde ise, aynı sessionda tekrar tekrar görebilsin.

Kolay gelsin.

*/


})();



(function() {
    var mainObj = {
      attribute: {
        pageType: "",
        isLogin: false,
        basket: {
          amount: 0,
          count: 0
        },
        category: {
          categoryName: "",
          gender: ""
        },
        product: {
          productName: "",
          productCode: "",
          price: 0,
          color: "",
          size: ""
        }
  
      },
      methods: {
        isLogin: function() {
           
        },
        category: function(){
            mainObj.attribute.category.categoryName = $("#breadCrumbs_child_2 span").text().trim()
            mainObj.attribute.category.gender =$("#breadCrumbs_child_2 span").text().trim().split(" ")[0]
 
        },
        isLogin: function(){
            mainObj.attribute.isLogin = ($("li.register").length == 0) ? true : false;
 
        },
        basket: function(){
            var a = $("li.basket ul li").length;
            var y = $("div.hb-sum b").text();
            mainObj.attribute.basket.amount = parseFloat(y);
            mainObj.attribute.basket.count = a;
        },
        product: function(){
            if(mainObj.attribute.pageType === "Product"){
               mainObj.attribute.product.productName = $("h1.black-v1").text().trim();
               var c = $("ins.price-payable").text();
               mainObj.attribute.category.categoryName = $("#breadCrumbs_child_2 span").text().trim()
               mainObj.attribute.category.gender =$("#breadCrumbs_child_2 span").text().trim().split(" ")[0]
               mainObj.attribute.product.price = parseFloat(c);
               mainObj.attribute.product.color = $("span.color").text();
               mainObj.attribute.product.productCode = $("ul.productInfo").text().split(":")[1].split(" ")[1]
               mainObj.attribute.product.size = $("ul.productInfo").text().split(":")[2].split(" ")[1]
               
            } 
             
        },
        pageTypee: function() {
          var x = $(location).attr('pathname').split("-");
            var y = $("h1.black-v1").text().trim().split(" ");
            console.log( y[0]);
          for (var i = 0; i < x.length; i++) {
              var str = x[i];
              switch(str){
                
                case '/':
                  mainObj.attribute.pageType = "Home";
                  return;
                case 'modelleri':
                  mainObj.attribute.pageType = "Listing";
                  return;
                case 'c':
                  mainObj.attribute.pageType = "Category";
                  return;
                case '/sepet':
                  mainObj.attribute.pageType = "Basket";
                  return;
                case '/' + y[0].toLowerCase():
                  mainObj.attribute.pageType = "Product";
                  return;
              }
          }
          
        },
        basketProduct: function(){
    
           var lastmousey=-1;
           var starttime = 0;          
           var distance = 0 ;
           var speed= 0;
           var yy = 0;
           var xx = 0;
           var checkSession = true;
           var html = '\
                      <div class="modal" id="myModal" style="display: none;position:fixed;z-index:1;padding-top:100px;left:0;top:0;width: 100%;height:100%;backround-color: rgb(0,0,0,0);backround-color: rgb(0,0,0,0.4)">\
                        <div class="modal-content" style="backround-color: #fefefe;margin:auto;padding:20px;border:1px solid #888; width:80%">\
                          <span class="close" style="color:#aaaaaa;float:right;font-size:28px;font-weight:bold">&times;</span>\
                          <p>Some text in the Modal..</p>\
                         </div>\
                      </div>\
                      ';
          jQuery('body').append(html);
              //kullanıcı product sayfasındaysa ve sepetinde 2 den fazla ürün varsa göster 
              if(mainObj.attribute.pageType === "Product" && mainObj.attribute.basket.count >= 2){
                  window.addEventListener('mousemove', function(e){
                    
                    //cursorun enson pozisyonu ile şimdiki pozisyonu arasında artma varsa yukarı hareket ediyordur 
                    if(lastmousey - e.y > 0){

                        console.log("x : " + e.x + " | y: " + e.y );
                        
                        lastmousetime = e.timeStamp;
                        //300 milisaniyede bir yukarı hareketi devam ediyormu diye bak 
                        if(e.timeStamp -starttime >300){
                          
                          //yukarı hareketi boyunca aldığı mesafeye bak
                          distance = Math.sqrt(Math.pow(e.y - yy,2) + Math.pow(e.x - xx,2));
                          speed = distance / (e.timeStamp -starttime);
                          // eğer yukarı hareket hızı 0.75 den büyükse popup çıkart
                          if(speed > 0.75){
                            var modal = document.getElementById("myModal");
                            var span = document.getElementsByClassName("close")[0];
                            modal.style.display = "block";
                            span.onclick = function() {
                                  modal.style.display = "none";
                            }
                            window.onclick = function(event) {
                              if (event.target == modal) {
                                modal.style.display = "none";
                              }
                            }
                            //alert( "asdfasdfasdfasdf")
                          }
                        }
                    }else{
                      distance = 0;
                      yy = e.y;
                      xx = e.x;
                      starttime = e.timeStamp;
                    }     
                         
                    lastmousey = e.y;

                
                
              })
             }
              
        },
        isPrepare: function() {
          try{
              mainObj.methods.pageTypee();
              mainObj.methods.basket();
              mainObj.methods.basketProduct();              
              mainObj.methods.category();
              mainObj.methods.product();             
              mainObj.methods.isLogin();
          }catch(e){
              //console.error(e);
              //return;
          }

          
        }
      }
    }
  
    mainObj.methods.isPrepare();
    window.saint = mainObj;
  
  })();
